FROM ubuntu:16.04
MAINTAINER Stephan <stephan@truevolve.technology>
RUN apt-get update
RUN apt-get install -y rsync
RUN addgroup --gid 1000 ubuntu
RUN adduser --system --uid 1000 --gid 1000 rsyncuser
USER rsyncuser
ENTRYPOINT ["rsync"]
